const kPieces = {
  king:   { white: "&#9812;", black: "&#9818;"},
  queen:  { white: "&#9813;", black: "&#9819;"},
  rook:   { white: "&#9814;", black: "&#9820;"},
  bishop: { white: "&#9815;", black: "&#9821;"},
  knight: { white: "&#9816;", black: "&#9822;"},
  pawn:   { white: "&#9817;", black: "&#9823;"}
}

const kInitialBoard = [
  ['br', 'bkn', 'bb', 'bq', 'bki', 'bb', 'bkn', 'br'],
  ['bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp'],
  ['', '', '', '', '', '', '', ''],
  ['', '', '', '', '', '', '', ''],
  ['', '', '', '', '', '', '', ''],
  ['', '', '', '', '', '', '', ''],
  ['wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp'],
  ['wr', 'wkn', 'wb', 'wq', 'wki', 'wb', 'wkn', 'wr']
];



let gSelectedSlot = null;



function Slot(piece = "", player = "") {
  this.piece = piece; // king, queen, rook, bishop, knight, pawn
  this.player = player; // white, black
}



function createBoard() {
  let board;

  // Create a board and initialize the slots with
  // info from kInitialBoard
  board = _.map(kInitialBoard, (row) => {
    return _.map(row, (str) => {
      if (str === '') {
        return new Slot();
      }

      let player = (str[0] === 'b') ? 'black' : 'white';
      let piece = str.slice(1);

      switch (piece) {
        case 'ki': return new Slot('king', player); break;
        case 'kn': return new Slot('knight', player); break;
        case 'q': return new Slot('queen', player); break;
        case 'r': return new Slot('rook', player); break;
        case 'b': return new Slot('bishop', player); break;
        case 'p': return new Slot('pawn', player); break;
      }
    });
  });

  return board;
}

// Create an 8x8 HTML table
function createTable() {
  const createCols = () => {
    let html = '';

    for (let i = 0; i < 8; ++i) {
      html += '<td></td>'; 
    }

    return html;
  };

  const createRows = () => {
    let html = '';

    for (let i = 0; i < 8; ++i) {
      html += '<tr>' + createCols() + '</tr>';
    }

    return html;
  };

  return "<table><tbody>" + createRows() + "</tbody></table>";
}

// Render the HTML table
$('#game').html(createTable());

let gBoard = createBoard();

// Go through the board and table and set the board slot's $elem
// to the appropriate HTML element
gBoard = _.zipWith(gBoard, $('tbody').children(), (boardRow, tableRow) => {
  return _.zipWith(boardRow, $(tableRow).children(), (boardCol, tableCol) => {
    let $col = $(tableCol);

    boardCol.$elem = $col;
    $col.on('click', onSlotClick.bind(boardCol));

    if (boardCol.piece !== '') {
      $col.html(kPieces[boardCol.piece][boardCol.player]);
    }

    return boardCol;
  });
});

// Clear all td's from class 'highlight'
function clearHighlights() {
  $('td.highlight').removeClass('highlight');
}

function onSlotClick() {
  // If we clicked on selected slot, clear selection
  if (gSelectedSlot === this) {
    clearHighlights();
    gSelectedSlot = null;
    return;
  }
  // Clear old selection's highlight
  else if (gSelectedSlot !== null) {
    clearHighlights();
  }

  this.$elem.addClass('highlight');
  gSelectedSlot = this;
}

// If we click away from the board, clear selection
$('body').on('click', (event) => {
  // Clicking on td counts as clicking inside body
  // so make sure we aren't clicking on td
  if (event.target.localName === 'td') {
    return;
  }
  if (gSelectedSlot !== null) {
    clearHighlights();
    gSelectedSlot = null;
  }
});
